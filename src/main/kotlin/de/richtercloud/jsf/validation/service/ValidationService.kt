/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.jsf.validation.service

import com.natpryce.hamkrest.Matcher
import kotlin.Throws
import java.io.IOException
import org.xml.sax.SAXException
import java.io.InputStream
import java.net.URI

interface ValidationService {
    @Throws(SAXException::class, IOException::class)
    fun validate(validationURI: URI): List<String>

    @Throws(SAXException::class, IOException::class)
    fun validate(inputStream: InputStream): List<String>

    @Throws(SAXException::class, IOException::class)
    fun validateMessages(validationURI: URI): List<ValidatorMessage>

    @Throws(SAXException::class, IOException::class)
    fun validateMessages(inputStream: InputStream): List<ValidatorMessage>

    @Throws(SAXException::class, IOException::class)
    fun validateMessages(
        validationURI: URI,
        minimumLevel: ValidatorMessageSeverity,
    ): List<ValidatorMessage>

    @Throws(SAXException::class, IOException::class)
    fun validateMessages(
        inputStream: InputStream,
        minimumLevel: ValidatorMessageSeverity,
    ): List<ValidatorMessage>

    @Throws(SAXException::class, IOException::class)
    fun validateMessages(
        validationURI: URI,
        minimumLevel: ValidatorMessageSeverity,
        ignore: Matcher<ValidatorMessage>,
    ): List<ValidatorMessage>

    @Throws(SAXException::class, IOException::class)
    fun validateMessages(
        inputStream: InputStream,
        minimumLevel: ValidatorMessageSeverity,
        ignore: Matcher<ValidatorMessage>,
    ): List<ValidatorMessage>

    companion object {
        fun transformValidatorResponse(messages: List<ValidatorMessage>): List<String> {
            return messages.map { it.toString() }
        }

        const val VALIDATOR_URL = "http://validator.w3.org/check"
        const val VALIDATION_OK = "Validation OK"
    }
}