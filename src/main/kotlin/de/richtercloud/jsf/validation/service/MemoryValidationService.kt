/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.jsf.validation.service

import kotlin.jvm.JvmOverloads
import nu.validator.client.EmbeddedValidator
import java.util.HashMap
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.natpryce.hamkrest.MatchResult
import com.natpryce.hamkrest.Matcher
import com.natpryce.hamkrest.nothing
import kotlin.Throws
import java.io.IOException
import org.slf4j.LoggerFactory
import org.xml.sax.SAXException
import java.io.InputStream
import java.net.URI

/**
 * Uses an embedded instance of Nu validator supported by W3C to validate HTML
 * code.
 *
 * `com.bitplan:w3cValidator:0.0.4` provides easy access to the validation
 * service hosted by W3C. However, this service can't be used for memory-based
 * implementations and possibly others since it forbids access after too frequent
 * requests.
 *
 * It's unclear how the W3C validator from http://validator.w3.org/source/ can be
 * build and used locally; there're system package provided for popular OS.
 *
 * @author richter
 */
class MemoryValidationService @JvmOverloads constructor(
    var isEnableCache: Boolean,
    private val embeddedValidator: EmbeddedValidator = EmbeddedValidator()
) : ValidationService {

    /**
     * Validation at every reload can be time consuming and non-constructive,
     * therefore this cache allows to skip unnecessary validations.
     *
     * This stores [ValidatorMessage] since transformation into
     * strings is considered trivial.
     */
    private val cache: MutableMap<URI, List<ValidatorMessage>?> = HashMap()
    private val objectMapper = jacksonObjectMapper()

    @Throws(SAXException::class, IOException::class)
    override fun validateMessages(
        validationURI: URI,
        minimumLevel: ValidatorMessageSeverity,
        ignore: Matcher<ValidatorMessage>
    ): List<ValidatorMessage> {
        var retValue = cache[validationURI]
        LOGGER.trace("cached value is: $retValue")
        if (!isEnableCache || retValue == null) {
            retValue = validateMessages(validationURI.toURL().openStream())
            LOGGER.trace("validation result: $retValue")
            cache[validationURI] = retValue
        }
        return retValue
            .filter { it.type.severityValue >= minimumLevel.severityValue }
            .filter { ignore.invoke(it) is MatchResult.Mismatch }
    }

    @Throws(SAXException::class, IOException::class)
    override fun validateMessages(
        inputStream: InputStream,
        minimumLevel: ValidatorMessageSeverity,
        ignore: Matcher<ValidatorMessage>
    ): List<ValidatorMessage> {
        val validatorResponseString = embeddedValidator.validate(inputStream)
        val validatorResponse = objectMapper.readValue(
            validatorResponseString,
            EmbeddedValidatorResponse::class.java
        )
        val retValue = transformEmbeddedMessages(validatorResponse.messages)
        return retValue
            .filter { it.type.severityValue >= minimumLevel.severityValue }
            .filter { ignore.invoke(it) is MatchResult.Mismatch }
    }

    @Throws(SAXException::class, IOException::class)
    override fun validateMessages(
        validationURI: URI,
        minimumLevel: ValidatorMessageSeverity
    ): List<ValidatorMessage> {
        return validateMessages(
            validationURI,
            minimumLevel,
            com.natpryce.hamkrest.nothing
        )
    }

    @Throws(SAXException::class, IOException::class)
    override fun validateMessages(
        inputStream: InputStream,
        minimumLevel: ValidatorMessageSeverity
    ): List<ValidatorMessage> {
        return validateMessages(
            inputStream,
            minimumLevel,
            nothing
        )
    }

    @Throws(SAXException::class, IOException::class)
    override fun validateMessages(validationURI: URI): List<ValidatorMessage> {
        return validateMessages(
            validationURI,
            ValidatorMessageSeverity.MINIMUM
        )
    }

    @Throws(SAXException::class, IOException::class)
    override fun validateMessages(inputStream: InputStream): List<ValidatorMessage> {
        return validateMessages(
            inputStream,
            ValidatorMessageSeverity.MINIMUM
        )
    }

    /**
     *
     * @param inputStream the reference to the content to be validated
     * @return the validation result if the validation failed or
     * [.VALIDATION_OK] if the validation succeeded
     * @throws IOException if an I/O error occurs
     * @throws org.xml.sax.SAXException if a SAX configuration error occurs
     */
    @Throws(IOException::class, SAXException::class)
    override fun validate(inputStream: InputStream): List<String> {
        val validatorResponseMessages = validateMessages(inputStream)
        val retValue: List<String> = ValidationService.transformValidatorResponse(validatorResponseMessages)
        LOGGER.trace("validation result: $retValue")
        return retValue
    }

    @Throws(IOException::class, SAXException::class)
    override fun validate(validationURI: URI): List<String> {
        LOGGER.trace("validating $validationURI")
        val validatorResponseMessages = validateMessages(validationURI)
        //uses cache
        return ValidationService.transformValidatorResponse(validatorResponseMessages)
    }

    private fun transformEmbeddedMessages(messages: List<EmbeddedValidatorResponseMessage>): List<ValidatorMessage> {
        return messages.map {
            ValidatorMessage(
                ValidatorMessageSeverity.convert(it.type),
                it.lastLine,
                it.lastColumn,
                it.firstColumn,
                it.message,
                it.extract,
                it.hiliteStart,
                it.hiliteLength
            )
        }
    }

    /**
     * Clears the cached value for the specified already cached URI. Note that
     * the same resource might be referred to through different URIs so that it
     * might be more convenient to clear the complete cache with
     * [.clearCache].
     *
     * @param cachedURI the URI for which to clear the cache
     */
    fun clearCacheForURI(cachedURI: URI) {
        cache.remove(cachedURI)
    }

    fun clearCache() {
        cache.clear()
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(MemoryValidationService::class.java)
    }
}