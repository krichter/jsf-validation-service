/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.jsf.validation.service

import com.natpryce.hamkrest.MatchResult
import com.natpryce.hamkrest.Matcher
import java.lang.IllegalArgumentException

class MessageValidatorMessageMatcher(private val message: String) : Matcher<ValidatorMessage> {

    /**
     * Performs the match check. The argument is `Object` instead of
     * `ValidatorMessage` because that the type used in the parent
     * interface [org.hamcrest.Matcher].
     *
     * @param actual the validator message to compare to
     * @return `true` if the message matches, `false` otherwise
     * @throws IllegalArgumentException if `actual` is not an instance of
     * [ValidatorMessage]
     */
    override fun invoke(actual: ValidatorMessage): MatchResult {
        if (actual !is ValidatorMessage) {
            return MatchResult.Mismatch("")
        }
        return if (actual.message == message) MatchResult.Match else MatchResult.Mismatch("")
    }

    override val description = "message: $message"
}