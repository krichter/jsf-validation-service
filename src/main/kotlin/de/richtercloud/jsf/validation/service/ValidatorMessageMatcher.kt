/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.jsf.validation.service

import com.natpryce.hamkrest.MatchResult
import com.natpryce.hamkrest.Matcher

class ValidatorMessageMatcher(private val validatorMessage: ValidatorMessage) : Matcher<ValidatorMessage> {

    override fun invoke(actual: ValidatorMessage): MatchResult {
        return if (actual == validatorMessage) MatchResult.Match else MatchResult.Mismatch("")
    }

    override val description: String = "validatorMessage: $validatorMessage"
}