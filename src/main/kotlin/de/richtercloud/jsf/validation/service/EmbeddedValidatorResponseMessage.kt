/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.jsf.validation.service

/**
 * Serves to deserialize responses from embedded validator.
 *
 * @author richter
 */
data class EmbeddedValidatorResponseMessage(
    var type: String? = null,
    var subType: String? = null,
    var lastLine: Int = 0,
    var lastColumn: Int = 0,
    var firstColumn: Int = 0,
    var message: String? = null,
    var extract: String? = null,
    var hiliteStart: Int = 0,
    var hiliteLength: Int = 0,
)