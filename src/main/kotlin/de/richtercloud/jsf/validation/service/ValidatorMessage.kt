/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.jsf.validation.service

import java.util.Objects

/**
 * Provides an interface to retrieve information about validator messages which
 * can be used by different validation providers/validator implementations.
 *
 * @author richter
 */
data class ValidatorMessage(
    var type: ValidatorMessageSeverity,
    var lastLine: Int = 0,
    var lastColumn: Int = 0,
    var firstColumn: Int = 0,
    var message: String? = null,
    var extract: String? = null,
    var hiliteStart: Int = 0,
    var hiliteLength: Int = 0,
) {

    override fun toString(): String = "type: $type, lastLine: $lastLine, lastColumn: $lastColumn, " +
            "firstColumn: $firstColumn, message: $message, extract: $extract, hiliteStart: $hiliteStart, " +
            "hiliteLength: $hiliteLength"

    override fun hashCode(): Int {
        var hash = 7
        hash = 67 * hash + Objects.hashCode(type)
        hash = 67 * hash + lastLine
        hash = 67 * hash + lastColumn
        hash = 67 * hash + firstColumn
        hash = 67 * hash + hiliteStart
        hash = 67 * hash + hiliteLength
        return hash
    }

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null) {
            return false
        }
        if (javaClass != obj.javaClass) {
            return false
        }
        val other = obj as ValidatorMessage
        if (lastLine != other.lastLine) {
            return false
        }
        if (lastColumn != other.lastColumn) {
            return false
        }
        if (firstColumn != other.firstColumn) {
            return false
        }
        if (hiliteStart != other.hiliteStart) {
            return false
        }
        return if (hiliteLength != other.hiliteLength) {
            false
        } else type == other.type
    }
}