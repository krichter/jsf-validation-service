/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.jsf.validation.service.web

import com.natpryce.hamkrest.Matcher
import javax.faces.component.FacesComponent
import javax.faces.component.UINamingContainer
import de.richtercloud.jsf.validation.service.ValidatorMessage
import javax.inject.Inject
import javax.annotation.PostConstruct
import javax.faces.context.FacesContext
import de.richtercloud.jsf.validation.service.ValidatorMessageSeverity
import org.xml.sax.SAXException
import java.io.IOException
import java.lang.Exception
import java.net.URISyntaxException

@FacesComponent("validationList")
class ValidationListComponent : UINamingContainer() {
    var validatorMessages: List<ValidatorMessage?>? = null

    @Inject
    private lateinit var validation: Validation
    var failure: Exception? = null

    @PostConstruct
    private fun init() {
        try {
            validatorMessages = validation.validate(
                getValueExpression("minimumLevel").getValue(FacesContext.getCurrentInstance().elContext) as ValidatorMessageSeverity,
                getValueExpression("from").getValue(FacesContext.getCurrentInstance().elContext) as Matcher<ValidatorMessage>
            )
        } catch (ex: IOException) {
            failure = ex
        } catch (ex: URISyntaxException) {
            failure = ex
        } catch (ex: SAXException) {
            failure = ex
        }
    }
}