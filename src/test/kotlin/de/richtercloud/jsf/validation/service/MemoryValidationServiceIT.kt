/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.jsf.validation.service

import kotlin.Throws
import java.net.URISyntaxException
import java.io.IOException
import java.net.InetSocketAddress
import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import com.sun.net.httpserver.HttpServer
import org.junit.Assert
import org.junit.Test
import org.xml.sax.SAXException
import java.net.URI

class MemoryValidationServiceIT {

    @Test
    fun testValidateNoFailures() {
        var server: HttpServer? = null
        try {
            server = HttpServer.create(InetSocketAddress(PORT), 0)
            val response = """<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"><head>
        <title>Facelet Title</title></head><body>
        Hello from Facelets
    </body>
</html>"""
            server.createContext("/test", HttpHandler { t: HttpExchange ->
                t.sendResponseHeaders(200, response.length.toLong())
                t.responseBody.use { os -> os.write(response.toByteArray()) }
            })
            server.executor = null // creates a default executor
            server.start()
            val validationURI = URI(
                String.format(
                    "http://localhost:%d/test",
                    PORT
                )
            )
            val instance = MemoryValidationService(false)
            val result = instance.validate(validationURI)
            Assert.assertEquals(
                "no validation failures expected for valid input",
                "[]",
                result.toString()
            )
        } finally {
            server?.stop(0)
        }
    }

    @Test
    fun testValidateFailureNestedForms() {
        var server: HttpServer? = null
        try {
            server = HttpServer.create(InetSocketAddress(PORT), 0)
            val response = """<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"><head>
        <title>Facelet Title</title></head><body>
        <form><form></form></form>
    </body>
</html>"""
            server.createContext("/test", HttpHandler { t: HttpExchange ->
                t.sendResponseHeaders(200, response.length.toLong())
                t.responseBody.use { os -> os.write(response.toByteArray()) }
            })
            server.executor = null // creates a default executor
            server.start()
            val validationURI = URI(
                String.format(
                    "http://localhost:%d/test",
                    PORT
                )
            )
            val instance = MemoryValidationService(false)
            val result = instance.validate(validationURI)
            Assert.assertEquals(
                "validation failures expected for nested forms in input",
                """[type: ERROR, lastLine: 4, lastColumn: 20, firstColumn: 15, message: Saw a “form” start tag, but there was already an active “form” element. Nested forms are not allowed. Ignoring the tag., extract:     <form><form></form, hiliteStart: 10, hiliteLength: 6, type: ERROR, lastLine: 4, lastColumn: 34, firstColumn: 28, message: Stray end tag “form”., extract: rm></form></form>
    <, hiliteStart: 10, hiliteLength: 7]""",
                result.toString()
            )
            //nested forms create more than one error message
        } finally {
            server?.stop(0)
        }
    }

    @Test
    fun testValidateLowercaseDoctype() {
        var server: HttpServer? = null
        try {
            server = HttpServer.create(InetSocketAddress(PORT), 0)
            val response = """<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"><head>
        <title>Facelet Title</title></head><body>
        Hello from Facelets
    </body>
</html>"""
            server.createContext("/test", HttpHandler { t: HttpExchange ->
                t.sendResponseHeaders(200, response.length.toLong())
                t.responseBody.use { os -> os.write(response.toByteArray()) }
            })
            server.executor = null // creates a default executor
            server.start()
            val validationURI = URI(
                String.format(
                    "http://localhost:%d/test",
                    PORT
                )
            )
            val instance = MemoryValidationService(false)
            val result = instance.validate(validationURI)
            Assert.assertEquals(
                "no validation failures expected for valid input",
                "[]",
                result.toString()
            )
        } finally {
            server?.stop(0)
        }
    }

    companion object {
        private const val PORT = 8000
    }
}