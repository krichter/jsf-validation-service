/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.jsf.validation.service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import kotlin.Throws
import java.io.IOException
import nu.validator.client.EmbeddedValidator
import org.mockito.Mockito
import java.io.ByteArrayInputStream
import org.junit.Assert
import org.junit.Test
import org.xml.sax.SAXException

class MemoryValidationServiceTest {

    @Test
    fun testValidateMessagesIrrelevantMessage() {
        //arrange
        val embeddedValidator = Mockito.mock(EmbeddedValidator::class.java)
        val instance = MemoryValidationService(
            false,
            embeddedValidator
        )
        val embeddedValidatorResponseMessage = EmbeddedValidatorResponseMessage("info")
        val embeddedValidatorResponse = EmbeddedValidatorResponse(listOf(embeddedValidatorResponseMessage))
        embeddedValidatorResponseMessage.type = "info"
        val objectMapper = jacksonObjectMapper()
        val embeddedValidatorResponseSerialized = objectMapper.writeValueAsString(embeddedValidatorResponse)
        val pageSourceInputStream = ByteArrayInputStream("page source".toByteArray())
        Mockito.doReturn(embeddedValidatorResponseSerialized).`when`(embeddedValidator).validate(pageSourceInputStream)

        //act
        val result = instance.validateMessages(pageSourceInputStream, ValidatorMessageSeverity.WARNING)

        //assert
        Assert.assertEquals(emptyList<Any>(), result)
    }

    @Test
    fun testValidateMessagesRelevantMessage() {
        //arrange
        val embeddedValidator = Mockito.mock(EmbeddedValidator::class.java)
        val instance = MemoryValidationService(
            false,
            embeddedValidator
        )
        val embeddedValidatorResponseMessage = EmbeddedValidatorResponseMessage("warn")
        val embeddedValidatorResponse = EmbeddedValidatorResponse(listOf(embeddedValidatorResponseMessage))
        val objectMapper = jacksonObjectMapper()
        val embeddedValidatorResponseSerialized = objectMapper.writeValueAsString(embeddedValidatorResponse)
        val pageSourceInputStream = ByteArrayInputStream("page source".toByteArray())
        Mockito.doReturn(embeddedValidatorResponseSerialized).`when`(embeddedValidator).validate(pageSourceInputStream)

        //act
        val result = instance.validateMessages(pageSourceInputStream)

        //assert
        Assert.assertEquals(
            listOf(ValidatorMessage(ValidatorMessageSeverity.WARNING, 0, 0, 0, message = null, extract = null, 0, 0)),
            result
        )
    }

    @Test
    fun testValidateMessagesIgnoredByMatcher() {
        //arrange
        val embeddedValidator = Mockito.mock(EmbeddedValidator::class.java)
        val instance = MemoryValidationService(
            false,
            embeddedValidator
        )
        val message = "Start tag seen without seeing a doctype first. Expected “<!DOCTYPE html>”."
        val embeddedValidatorResponseMessage = EmbeddedValidatorResponseMessage("warn", message = message)
        val embeddedValidatorResponse = EmbeddedValidatorResponse(listOf(embeddedValidatorResponseMessage))
        val objectMapper = jacksonObjectMapper()
        val embeddedValidatorResponseSerialized = objectMapper.writeValueAsString(embeddedValidatorResponse)
        val pageSourceInputStream = ByteArrayInputStream("page source".toByteArray())
        Mockito.doReturn(embeddedValidatorResponseSerialized).`when`(embeddedValidator).validate(pageSourceInputStream)

        //act
        val result = instance.validateMessages(
            pageSourceInputStream,
            ValidatorMessageSeverity.WARNING,
            MessageValidatorMessageMatcher(message)
        )

        //assert
        Assert.assertEquals(
            emptyList<Any>(),
            result
        )
    }
}